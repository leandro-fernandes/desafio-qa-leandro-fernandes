require 'test-unit'

#Stores scanned itens and calculates the total value
class CheckOut

  def initialize(rules=[])
    @rules = rules
    @itens = Hash.new
  end

  #Checks if the item is already on the hash and count the amount
  def scan(item)
    (@itens.has_key? item) ? (@itens[item] += 1) : (@itens[item] = 1)
  end

  #Calculates the total value of the itens
  def total
    checkout_total = 0
    @itens.each do |item, amount|
      while amount > 0
        #Sum the item price according to its rule to the checkout_total variable
        rule = get_rule(item, amount)
        checkout_total += rule.price
        amount -= rule.amount
      end
    end
    checkout_total
  end

  #Search the correct rule for the item sorting by amount
  def get_rule(item, amount)
    rules = @rules.select{ |rule| rule.item == item }.sort_by{ |r| r.amount }.reverse!
    return rules.select{ |rule| rule.amount <= amount }.first
  end

end

#Rule containing the item's name, price and amount
class Rule
  attr_accessor :item, :price, :amount

  def initialize(item, price, amount=1)
    @item = item
    @price = price
    @amount = amount
  end

end

RULES = [Rule.new('A',50),
         Rule.new('A',130,3),
         Rule.new('B',30),
         Rule.new('B',45,2),
         Rule.new('C',20),
         Rule.new('D',15)]

class TestPrice < Test::Unit::TestCase

  def price(goods)
    co = CheckOut.new(RULES)
    goods.split(//).each { |item| co.scan(item) }
    co.total
  end

  def test_totals
    assert_equal(  0, price(""))
    assert_equal( 50, price("A"))
    assert_equal( 80, price("AB"))
    assert_equal(115, price("CDBA"))

    assert_equal(100, price("AA"))
    assert_equal(130, price("AAA"))
    assert_equal(180, price("AAAA"))
    assert_equal(230, price("AAAAA"))
    assert_equal(260, price("AAAAAA"))

    assert_equal(160, price("AAAB"))
    assert_equal(175, price("AAABB"))
    assert_equal(190, price("AAABBD"))
    assert_equal(190, price("DABABA"))
  end

  def test_incremental
    co = CheckOut.new(RULES)
    assert_equal( 0, co.total)
    co.scan("A"); assert_equal( 50, co.total)
    co.scan("B"); assert_equal( 80, co.total)
    co.scan("A"); assert_equal(130, co.total)
    co.scan("A"); assert_equal(160, co.total)
    co.scan("B"); assert_equal(175, co.total)
  end
end