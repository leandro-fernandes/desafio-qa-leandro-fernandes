Feature: Messaging contacts

Scenario: Sending a text message
Given that the user has contacts in his contacts list
And there is internet connectivity available
When the user selects a contact
And the user writes a text message
And the user sends the message to the contact
Then the message sent check marks appears successfully

Scenario: Sending a sound message
Given that the user has contacts in his contacts list
And there is internet connectivity available
When the user selects a contact
And the user records a sound message
And the user sends the message to a contact
Then the message sent check marks appears successfully